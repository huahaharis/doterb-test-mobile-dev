import Types from '../../../utils/Types';
import axios from 'axios'

export const inputName = (username, password)=>{
    // console.log(name);
    return(dispatch)=>{
        axios.post('http://34.101.221.232:3000/api/v1/oauth/token',
        {
          'username': username,
          'password': password
        }
      )
        .then((result) => {
          console.log("RES LOGIN ", JSON.stringify(result,null,2));
          dispatch(inputNameSuccess(result.data))
        })
        .catch((error) => {
          console.log("error LOGIN ", JSON.stringify(error,null,2));
        })
    }
}

const inputNameSuccess =(data)=>{
    return {
        type: Types.INPUT_NAME,
        data
      }
}