import Types from '../../../utils/Types';

export const selectData = (id, name)=>{
    console.log(name);
    const data ={
        id: id,
        name: name,
    }
    return(dispatch)=>{
        dispatch(selectDataSuccess(data))
    }
}

const selectDataSuccess =(data)=>{
    return {
        type: Types.SELECT_DATA,
        data
      }
}