import Types from '../../../utils/Types';

const INITIAL_STATE = {
  Response: null
}

const LoginReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case Types.INPUT_NAME:
        state = {
          ...state,
          Response: action.data
        }
        break
      default:
        break
    }
  
    return state
  }

  export default LoginReducers