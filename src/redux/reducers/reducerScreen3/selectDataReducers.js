import Types from '../../../utils/Types';

const INITIAL_STATE = {
  data: {}
}

const selectDataReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case Types.SELECT_DATA:
        state = {
          ...state,
          data: action.data
        }
        break
      default:
        break
    }
  
    return state
  }

  export default selectDataReducers