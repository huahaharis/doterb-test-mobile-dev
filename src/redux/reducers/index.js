import {combineReducers} from 'redux';
import LoginReducers from './reducerScreen1/loginReducers';
import PickDataReducer from './reducerScreen3/selectDataReducers'

export default combineReducers({
    LoginReducers,
    PickDataReducer
})