import AsyncStorage from '@react-native-async-storage/async-storage'

export const getKey = async (key) => {
    let data = ''
    try {
        data = await AsyncStorage.getItem(key)
    } catch (error) {
        console.log(error.message)
    }
    return data
}

export const setKey = async (key, value) => {
    try {
        await AsyncStorage.setItem(key, value)
    } catch (error) {
        console.log(error.message)
    }
}

export const removeKey = async (key) => {
    try {
        await AsyncStorage.removeItem(key)
        console.log('key removed')
    } catch (error) {
        console.log(error.message)
    }
}


