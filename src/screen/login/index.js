import React from 'react';
import {
  View, 
  Text, 
  StyleSheet, 
  TextInput, 
  TouchableOpacity, 
  ImageBackground,
  Dimensions,
  Image,
  StatusBar,
  ToastAndroid
} from 'react-native';
import * as actions from '../../redux/actions/index';
import {useDispatch, useSelector} from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Entypo';
import {getKey, setKey} from '../../utils/shared-pref';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const LoginScreen = () => {
  const navigation = useNavigation()
  const dispatch = useDispatch()
  const [username, setUserame]= React.useState('')
  const [password, setPassword]= React.useState('')
  const [press, setPress] = React.useState(false)
  const redux = useSelector(state => state)

  const inputName = (username, password) => {

    dispatch(actions.inputName(username, password))
  }


  React.useEffect(async()=>{
    if(redux.LoginReducers?.Response == null){
      console.log('belom login');
    } else {
      let token = redux.LoginReducers.Response.result
      setKey('token', token)
      navigation.navigate('lihatmateri')
    }
  },[redux.LoginReducers?.Response])
  return (
    <View style={style.container}>
      <StatusBar hidden />
      <ImageBackground
        source={require('../../components/bg.jpeg')}
        style={{width: windowWidth, height: '60%'}}></ImageBackground>
      <View style={style.halfUp}>
        <Image
          source={require('../../components/DotERB.jpeg')}
          style={{
            height: '30%',
            width: '76%',
            marginBottom: 30,
            borderRadius: 10,
          }}
        />
        <TextInput
          style={style.inputField}
          onChangeText={text => setUserame(text)}
          value={username}
          editable={true}
          maxLength={40}
          multiline={false}
          placeholder="Username"
        />
        <View style={style.inputField2}>
          <TextInput
            onChangeText={text => setPassword(text)}
            value={password}
            editable={true}
            maxLength={20}
            multiline={false}
            secureTextEntry={press === false ? true : false}
            placeholder="Password"
          />
          <TouchableOpacity 
          style={{marginRight: '5%', marginTop: '8.4%'}}
          onPress={()=>setPress(!press)}>
            {press === false ? (
              <Icon name="eye" size={20} />
            ) : (
              <Icon name="eye-with-line" size={20}/>
            )}
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={style.button}
          onPress={() => inputName(username, password)}>
          <Text
            style={{
              alignSelf: 'center',
              top: 2,
              fontWeight: 'bold',
            }}>
            Submit
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  halfUp: {
    backgroundColor: '#F1E9E7',
    elevation: 10,
    shadowOpacity: 4,
    width: windowWidth / 1.5,
    height: '50%',
    borderRadius: 15 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: '16.2%',
    marginTop: '-49%',
  },
  inputField: {
    width: '82%',
    height: '15%',
    fontSize: 14,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    paddingLeft: 10,
  },
  inputField2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '82%',
    height: '15%',
    fontSize: 14,
    borderRadius: 10,
    // borderWidth: 1,
    // borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    paddingLeft: 10,
    marginTop: '3%',
  },
  button: {
    width: windowWidth * 0.5,
    height: windowHeight * 0.04,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#ffffff',
    marginTop: 25,
  },
  button2: {
    width: '32%',
    height: '65%',
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#F99C5E',
    paddingLeft: 10,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 5,
  },
  searchSection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width: '75%',
    height: 40,
    marginHorizontal: '7.5%',
    borderRadius: 15,
    margin: 10,
    paddingLeft: 2,
    paddingRight: 16,
  },
});

export default LoginScreen