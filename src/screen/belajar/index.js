import React from 'react';
import {
  View, 
  Text, 
  StyleSheet, 
  TouchableOpacity, 
  ImageBackground,
  Dimensions,
  Image,
  StatusBar,
  FlatList
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const BelajarScreen = () => {
  const navigation = useNavigation()

  return (
    <View style={style.container}>
      <StatusBar hidden />
      <ImageBackground
        source={require('../../components/assets/bg_bright.png')}
        style={{width: windowWidth, height: '60%'}}>
        <View style={{marginTop: '15%'}}>
          <View
            style={{
              height: '1.9%',
              width: '70%',
              backgroundColor: '#F99C5E',
              borderRadius: 60,
              marginHorizontal: '30%',
              top: '8%',
            }}
          />
          <View
            style={{
              height: '1.9%',
              width: '90%',
              backgroundColor: '#F99C5E',
              borderRadius: 60,
              marginHorizontal: '27%',
              marginVertical: '2%',
              top: '8%',
            }}
          />
          <View
            style={{
              height: '1.9%',
              width: '70%',
              backgroundColor: '#F99C5E',
              borderRadius: 60,
              marginHorizontal: '30%',
              top: '8%',
            }}
          />
          <Text style={{color: 'black', fontSize: 26}}>Hallo,</Text>
          <Text style={{color: '#F99C5E', fontSize: 26, fontWeight: 'bold'}}>
            Selamat Datang
          </Text>
        </View>
      </ImageBackground>
      <TouchableOpacity
      onPress={()=>navigation.navigate('lihatmateri')}
      style={style.button}>
        <Text>Lihat Materi</Text>
      </TouchableOpacity>
      <TouchableOpacity 
      onPress={()=>navigation.navigate('tambahmateri')}
      style={style.button2}>
        <Text>Tambah Materi</Text>
      </TouchableOpacity>
      <View style={{marginVertical: '-5%', height: '10%', marginTop: '79%'}}>
        <Image
          source={require('../../components/assets/img_bg_bottom.png')}
          style={{height: '80%', width: windowWidth, tintColor: 'grey'}}
        />
      </View>
    </View>
  );
}
const style = StyleSheet.create({
  container:{
    flex:1
  },
  halfUp:{
    backgroundColor: '#F8F5F3', 
    elevation: 10,
    shadowOpacity: 4,
    width: windowWidth /1.5, 
   height: '50%',
    borderRadius: 15/2, 
    justifyContent: 'center', 
    alignItems: 'center',
    marginHorizontal: '16.2%', 
    marginTop: '-49%'
  },
  button: {
    width: '62%',
    height: '8%',
    fontSize: 14,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#F99C5E',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: '18%',
    marginVertical: '-35%',
    marginTop: '-30%'
  },
  button2: {
    width: '62%',
    height: '8%',
    fontSize: 14,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#F99C5E',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: '18%',
    marginVertical: '5%',
    marginTop: '8%'
  },
})

export default BelajarScreen