import React from 'react'
import {View, Text, Image, StyleSheet, TouchableOpacity, TextInput, FlatList, Modal} from 'react-native';
import { Container, Header, Left, Body, Right, Button, Thumbnail, Card, CardItem, Content } from 'native-base';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Entypo';
import Icon2 from 'react-native-vector-icons/AntDesign';
import {getKey} from '../../utils/shared-pref';
import axios from 'axios';

const ListMateri = () => {
    const navigation = useNavigation()
    const [datas, setDatas]= React.useState([])
    const [press, setPress] = React.useState(false)

    React.useEffect(async()=>{
      const token = await getKey('token')
      axios
        .get(`http://34.101.221.232:3001/api/v1/sentra_belajars/`,{
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        .then(response => {
          setDatas(response.data.result.data)
          console.log(JSON.stringify(datas,null,2));
        })
        .catch(err => {
          console.log(err);
        });
    },[press])


    return (
      <View style={styles.container}>
        <View>
          <Text style={styles.h2text}>Materi</Text>
        </View>
        <FlatList
          data={datas}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => {
            return (
              <View style={{width: 400}}>
                <Card>
                  <CardItem>
                    <Left>
                      <Thumbnail source={{uri: `${item.image}`}} />
                      <Body>
                        <Text>Title: {item.title}</Text>
                        <Text note>Content: {item.content}</Text>
                      </Body>
                    </Left>
                  </CardItem>
                  <CardItem cardBody={true}>
                    <Left style={{marginLeft: '10%', marginBottom: '5%'}}>
                    <TouchableOpacity onPress={()=>navigation.navigate('tambahmateri')}>
                        <Icon2 name="addfile" size={20}  color={'black'} />
                      </TouchableOpacity>
                    </Left>
                    <Right style={{marginRight: '10%', marginBottom: '5%'}}>
                      <TouchableOpacity onPress={()=>navigation.navigate('tambahmateri',{title: item.title, content: item.content})}>
                        <Icon name="edit" size={18}  color={'black'} />
                      </TouchableOpacity>
                    </Right>
                  </CardItem>
                </Card>
              </View>
            );
          }}
        />
      </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    h2text: {
        marginTop: 10,
        fontFamily: 'Helvetica',
        fontSize: 36,
        fontWeight: 'bold',
    },
    flatview: {
        justifyContent: 'center',
        alignItems: "center",
        paddingTop: 30,
        borderRadius: 2,
    },
    name: {
        fontFamily: 'Verdana',
        fontSize: 18,
        alignItems: "center"
    },
    floatingbutton: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        position: "absolute",
        bottom: 10,
        right: 10,
        height: 60,
        backgroundColor: '#fff',
        borderRadius: 60,
    }, wrapper: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        height: "100%",
        width: "100%",
        zIndex: 9999
    },
    blurView: {
        flex: 1,
        zIndex: 99999
    },
    containerbaru: {
        margin: 25,
        zIndex: 99999,
        position: "relative",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    box: {
        backgroundColor: "white",
        padding: 20,
        position: "absolute",
        top: "15%",
        borderColor: "black",
        borderWidth: StyleSheet.hairlineWidth,
        margin: 25,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
        zIndex: 99999,
        width: "85%"
    },
    titleText: {
        fontFamily: "Lora",
        fontSize: 20
    },
    bodyText: {
        marginTop: 15,
        textAlign: "center",
        fontFamily: "Lora",
        fontSize: 16,
        lineHeight: 20
    },
    modalView: {
        margin: 10,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 80,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 10
    },
});

export default ListMateri