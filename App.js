import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Login from './src/screen/login/index';
import Belajar from './src/screen/belajar/index';
import TambahMateri from './src/screen/belajar/tambahMateri';
import ListMateri from './src/screen/belajar/listMateri';

const stack = createStackNavigator();

const App =()=>{
  return(
  <NavigationContainer>
    <stack.Navigator>
      <stack.Screen name="login" component={Login} options={{headerShown:false}}/>
      <stack.Screen name="belajar" component={Belajar} options={{headerShown:false}}/>
      <stack.Screen name="tambahmateri" component={TambahMateri} options={{headerShown:false}}/>
      <stack.Screen name="lihatmateri" component={ListMateri} options={{headerShown:false}}/>
    </stack.Navigator>
  </NavigationContainer>
  )
}

export default App;